
//     Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.

function createNewUser(newUser) {
    newUser = {};
    let firstName = prompt ('Enter name');
    let lastName = prompt ('Enter last name');
    newUser['first name'] = firstName;
    newUser['last name'] = lastName;
    newUser['getLogin'] = function() {
        return this['first name'][0].toLowerCase() + this['last name'].toLowerCase();
    };
    console.log(newUser);
    console.log(newUser.getLogin());
}
createNewUser();


