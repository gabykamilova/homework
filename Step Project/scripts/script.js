(function($) { //переключаемые табы
    $(function() {
        $("ul.tabs").on("click", "li:not(.active)", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.our-services")
                .find("div.tab-content")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
        });
    });
})(jQuery);


$(function() {  //filtering tabs

    $("ul.tabs2").on('click', 'li:not(.active)', function(){
        $(this).addClass("active").siblings().removeClass("active");
        let category = $(this).attr("id");
        $(".gallery>a").not("."+category).slideUp();
        $("."+category).slideDown();
    });
});

$(document).ready(function () { //Load more images
    $('.gallery>a:lt(12)').show();
    let items =  36;
    let shown =  12;
    $('#loadmore1').click(function () {
        shown = $('.gallery a:visible').length+12;
        if(shown< items) {
            $('.gallery a:lt('+shown+')').show(200);
        } else {
            $('.gallery a:lt('+items+')').show(200);
            $('#loadmore1').hide();
        }
    });
});


$(document).ready(function () { //Load more images 2
    $('.images-gallery li:lt(10)').show();
    let items =  30;
    let shown =  10;
    $('#loadmore2').click(function () {
        shown = $('.images-gallery li:visible').length+10;
        if(shown< items) {
            $('.images-gallery li:lt('+shown+')').show(200);
        } else {
            $('.images-gallery li:lt('+items+')').show(200);
            $('#loadmore2').hide();
        }
    });
});


$('.slider-for').slick({ //slider carousel
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: true,
    focusOnSelect: true
});


$(function(){
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });
});