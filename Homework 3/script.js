let num1 = null;
let num2 = null;
let oper = null;

while (num1 !== Number(num1) || num2 !== Number(num2) || num1 === '' || num2 === '') {
   num1 = +prompt ("Enter number 1");
   num2 = +prompt ("Enter number 2");
   oper = prompt ("Enter operation");
}

function calcNumbers (num1, num2, oper) {
   switch(oper) {
       case "+":
           console.log(num1 + num2);
           break;
       case "-":
           console.log(num1 - num2);
           break;
       case "*":
           console.log(num1 * num2);
           break;
       case "/":
           console.log(num1 / num2);
           break;
       default:
           alert("Enter correct operator");
           break;
   }
}
calcNumbers(num1, num2, oper);