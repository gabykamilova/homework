// Создать объект студент "студент" и проанализировать его табель.
//
//     Технические требования:
//
//     Создать пустой объект student, с полями name и last name.
//     Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
//     В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
//     Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
//     Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.


let getName = prompt ('Enter student name');
let getLastName = prompt ('Enter student last name');

const student = {
  name: getName,
  'last name': getLastName
};

student.tabel = {};
    let course = prompt('Enter course name');
    let mark = +prompt('Enter course mark');

    while (course) { //более краткий вариант while (course != 0)
        student.tabel[course] = mark;
        course = prompt('Enter course name');
        mark = +prompt('Enter course mark');
    }

    let badMarks = false;

    for (let key in student.tabel) {
        if (student.tabel[key] < 4) {
            badMarks = true;
            break;
        }
    }

    if (!badMarks) {
        console.log ('Student goes to next course');
        let averageMarks = 0;
        let courseCount = 0;

        for (let key in student.tabel) {
            courseCount++;
            averageMarks += student.tabel[key];
        }

        averageMarks = averageMarks/courseCount;

        if (averageMarks > 7) {
            console.log ('Student is selected for a scholarship');
        }
    };
    console.log (student);